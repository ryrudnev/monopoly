/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.game;

import java.util.Random;

/**
 * Определяет игровые кости.
 * @author Роман
 */
public class Dice {

    /**
     * Определяет количество выпавших очков на игровых костях.
     */
    public class DiceScore {

        /**
         * Количество очков выпавших на первой кости.
         */
        private final int _first;
        /**
         * Количество очков выпавших на второй кости.
         */
        private final int _second;

        /**
         * Задать количество выпавших очков.
         * @param first очки на первой игровой кости.
         * @param second очки на второй игровой кости.
         */
        public DiceScore(int first, int second) {
            _first = first;
            _second = second;
        }

        /**
         * Получить количество очков на первой игровой кости.
         * @return количество очков.
         */
        public int firstScore() {
            return _first;
        }

        /**
         * Получить количество очков на второй игровой кости.
         * @return количество очков.
         */
        public int secondScore() {
            return _second;
        }

        /**
         * Получить суммарное количество выпавших очков.
         * @return количество очков.
         */
        public int score() {
            return _first + _second;
        }
    }
    /**
     * Количество выпашвих очков.
     */
    private DiceScore _score = new DiceScore(0, 0);

    /**
     * Бросить игровые кости.
     */
    public void drop() {
        Random rand = new Random();
        _score = new DiceScore(rand.nextInt(6) + 1, rand.nextInt(6) + 1);
    }

    /**
     * Получить количество выпавших очков.
     * @return количество очков.
     */
    public DiceScore score() {
        return _score;
    }
}
