/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.personality;

import java.util.List;
import monopoly.model.resource.ILand;

/**
 * Определяет абстракцию земельного владельца. 
 * @author Роман
 */
public interface ILandOwner extends IMooneyOwner {

    /**
     * Уплатить ренту за земельный участок ее владельцу.
     * @param land земельный участок за которую платиться рента.
     * @return флаг, показывающий была ли уплачена рента.
     */
    public boolean payRent(ILand land);

    /**
     * Добавить во владения новый земельный участок.
     * @param land земельный участок.
     */
    public void addLand(ILand land);

    /**
     * Изъять из владений указанный земельный участок.
     * @param land земельный участок
     * @return возможно ли забрать указанный земельный участок у владельца.
     */
    public boolean removeLand(ILand land);

    /**
     * Получить все земельный владения. 
     * @return список земельных участков у владельца.
     */
    public List<ILand> landowning();
}
