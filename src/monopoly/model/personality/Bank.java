/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.personality;

import java.util.ArrayList;
import java.util.List;
import monopoly.model.resource.ILand;

/**
 * Определяет банк игры.
 * @author Роман
 */
public class Bank implements ILandSeller {

    /**
     * Земельный владения банка.
     */
    private List<ILand> _lands;

    /**
     * Задать банк.
     */
    public Bank() {
        _lands = new ArrayList<ILand>();
    }

    /**
     * Проверяет является ли игрок банкротом.
     * @param p игрок.
     * @return результат проверки.
     */
    public boolean isBankrupt(Player p) {
        return p.capital() == 0;
    }

    /**
     * Сделать банкротом игрока.
     * Все имущество игрока переходит в собственность банка.
     * @param p игрок.
     */
    public void doBankrupt(Player p) {
        p.takeMoney(p.money());

        for (ILand land : p.landowning()) {
            p.removeLand(land);
            addLand(land);
        }
    }

    /**
     * Отобрать у игрока указанную денежную сумму.
     * @param p игрок.
     * @param amount денежная сумма.
     * @return флаг, показываюший была ли изъята указанная сумма.
     */
    public boolean takeAwayMoney(Player p, int amount) {
        return p.takeMoney(amount);
    }

    /**
     * Дать игроку указанную денежную сумму.
     * @param p игрок.
     * @param amount денежная сумма.
     * @return флаг, показываюший была ли добавлена указанная сумма.
     */
    public boolean giveMoney(Player p, int amount) {
        return p.addMoney(amount);
    }

    /**
     * Конфисковать у игрока первый по списку земельный участок. 
     * @param p игрок.
     * @return конфискованный участок или null.
     */
    public ILand confiscateLand(Player p) {
        List<ILand> lands = p.landowning();
        if (lands.isEmpty()) {
            return null;
        }

        ILand confiscated = lands.get(0);
        p.removeLand(confiscated);
        p.addMoney(confiscated.confiscationCost());
        addLand(confiscated);

        return confiscated;
    }

    /**
     * Продать указанный участок земли.
     * @param buyer покупатель.
     * @param amount сумма сделки.
     * @param land продаваемый земельный участок.
     * @return успешность сделки.
     */
    public boolean sellLand(ILandBuyer buyer, int amount, ILand land) {
        if (buyer == this) {
            return false;
        }

        if (land.owner() != this || _lands.contains(land) == false) {
            return false;
        }

        if (buyer.takeMoney(amount) == false
                || removeLand(land) == false) {
            return false;
        }

        buyer.addLand(land);
        return true;
    }

    /**
     * Добавить во владения новый земельный участок.
     * @param land земельный участок.
     */
    public void addLand(ILand land) {
        if (land.owner() == this || _lands.contains(land)) {
            return;
        }
        land.setOwner(this);
        _lands.add(land);
    }

    /**
     * Изъять из владений указанный земельный участок.
     * @param land земельный участок
     * @return возможно ли забрать указанный земельный участок у владельца.
     */
    public boolean removeLand(ILand land) {
        if (land.owner() != this || _lands.remove(land) == false) {
            return false;
        }
        land.setOwner(null);

        return true;
    }

    /**
     * Получить все земельный владения. 
     * @return список земельных участков у банка.
     */
    public List<ILand> landowning() {
        return _lands;
    }

    /**
     * Получить наименование банка.
     * @return наименование.
     */
    public String name() {
        return "Банк";
    }

    /**
     * Получить денежную сумму.
     * @return денежная сумма.
     */
    public int money() {
        return Integer.MAX_VALUE;
    }

    /* Методы, находящиеся не в компетенции банка. */
    public boolean payRent(ILand land) {
        return true;
    }

    public boolean addMoney(int amount) {
        return true;
    }

    public boolean takeMoney(int amount) {
        return true;
    }
}
