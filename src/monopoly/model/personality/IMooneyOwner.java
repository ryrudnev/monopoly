/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.personality;

/**
 * Определяет абстракцию владельца игровых денег.
 * @author Роман
 */
public interface IMooneyOwner {

    /**
     * Получить сумму денег, который обладает владелец.
     * @return сумму денег.
     */
    public int money();

    /**
     * Добавить денежную сумму.
     * @param amount добавляемая денежная сумма.
     * @return флаг, показываюший была ли добавлена указанная сумма.
     */
    public boolean addMoney(int amount);

    /**
     * Забрать денежную сумму.
     * @param amount изымаемая денежная сумма.
     * @return флаг, показываюший была ли изъята указанная сумма.
     */
    public boolean takeMoney(int amount);

    /**
     * Получить имя владельца.
     * @return имя.
     */
    public String name();
}
