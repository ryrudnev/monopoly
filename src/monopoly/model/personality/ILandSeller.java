/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.personality;

import monopoly.model.resource.ILand;

/**
 * Определяет абстракцию игрового продовца земельных участков.
 * @author Роман
 */
public interface ILandSeller extends ILandOwner {

    /**
     * Продать указанный участок земли.
     * @param buyer покупатель.
     * @param amount сумма сделки.
     * @param land продаваемый земельный участок.
     * @return успешность сделки.
     */
    public boolean sellLand(ILandBuyer buyer, int amount, ILand land);
}
