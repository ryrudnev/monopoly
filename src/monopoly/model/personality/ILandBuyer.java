/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.personality;

import monopoly.model.resource.ILand;

/**
 *Определяет абстракцию игрового покупателя земельных участков.
 * @author Роман
 */
public interface ILandBuyer extends ILandOwner {

    /**
     * Купить указанный участок земли.
     * @param seller продавец.
     * @param amount сумма сделки.
     * @param land покупаемый земельный участок.
     * @return успешность сделки.
     */
    public boolean buyLand(ILandSeller seller, int amount, ILand land);
}
