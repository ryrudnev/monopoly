/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.personality;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import monopoly.model.game.Dice;
import monopoly.model.game.Dice.DiceScore;
import monopoly.model.gamefield.GameFieldCellBase;
import monopoly.model.resource.ILand;

/**
 * Определяет участника игры, управляемого человеком.
 * @author Роман
 */
public class Player implements ILandSeller, ILandBuyer {

    /**
     * Имя игрока.
     */
    private final String _name;
    /**
     * Цвет игрока.
     */
    private final Color _color;
    /**
     * Сумма имеющихся денег.
     */
    private int _money;
    /**
     * Земельный владения игрока.
     */
    private final List<ILand> _lands;
    /**
     * Позиция игрока на игровом поле.
     */
    private GameFieldCellBase _position;

    /**
     * Задать игрока.
     * @param name имя игрока.
     * @param color цвет игрока.
     * @param capital стартовый капитал.
     */
    public Player(String name, Color color, int capital) {
        _name = name;
        _color = color;
        _money = capital;
        _lands = new ArrayList<ILand>();
    }

    /**
     * Получить имя игрока.
     * @return имя.
     */
    public String name() {
        return _name;
    }

    /**
     * Получить цвет игрока.
     * @return цвет игрока.
     */
    public Color color() {
        return _color;
    }

    /**
     * Узнать капитал игрока в денежном эквиваленте.
     * @return капитал игрока.
     */
    public int capital() {
        int capital = _money;
        for (ILand land : _lands) {
            capital += land.confiscationCost();
        }

        return capital;
    }

    /**
     * Получить позицию игрока.
     * @return позиция игрока.
     */
    public GameFieldCellBase position() {
        return _position;
    }

    /**
     * Задать позицию игрока.
     * @param position позиция игрока.
     */
    public void setPosition(GameFieldCellBase position) {
        _position = position;
    }

    /**
     * Бросить игровые кости.
     * @param dice игровые кости.
     * @return количество выпавших очков на игровых костях.
     */
    public DiceScore dropDice(Dice dice) {
        dice.drop();
        return dice.score();
    }

    /**
     * Переместится на игровом поле на указанное количество шагов.
     * @param stepPoints количество шагов.
     */public void shiftOnField(int stepPoints) {
        while (stepPoints-- > 0) {
            _position = _position.next();
        }
    }

    /**
     * Продать указанный участок земли.
     * @param buyer покупатель.
     * @param amount сумма сделки.
     * @param land продаваемый земельный участок.
     * @return успешность сделки.
     */
    public boolean sellLand(ILandBuyer buyer, int amount, ILand land) {
        if (buyer == this) {
            return false;
        }

        if (land.owner() != this || _lands.contains(land) == false) {
            return false;
        }

        if (buyer.takeMoney(amount) == false
                || removeLand(land) == false
                || addMoney(amount) == false) {
            return false;
        }

        buyer.addLand(land);
        return true;
    }

    /**
     * Купить указанный участок земли.
     * @param seller продавец.
     * @param amount сумма сделки.
     * @param land покупаемый земельный участок.
     * @return успешность сделки.
     */
    public boolean buyLand(ILandSeller seller, int amount, ILand land) {
        if (seller == this) {
            return false;
        }

        if (land.owner() != seller
                || seller.landowning().contains(land) == false) {
            return false;
        }

        if (takeMoney(amount) == false
                || seller.removeLand(land) == false
                || seller.addMoney(amount) == false) {
            return false;
        }

        addLand(land);
        return true;
    }

    /**
     * Уплатить ренту за земельный участок ее владельцу.
     * @param land земельный участок за которую платиться рента.
     * @return флаг, показывающий была ли уплачена рента.
     */
    public boolean payRent(ILand land) {
        if (land.owner() == null || land.owner() == this) {
            return false;
        }

        int rent = land.rent();
        if (takeMoney(rent) == false) {
            return false;
        }

        return land.owner().addMoney(rent);
    }

    /**
     * Добавить во владения новый земельный участок.
     * @param land земельный участок.
     */
    public void addLand(ILand land) {
        if (land.owner() == this || _lands.contains(land)) {
            return;
        }
        land.setOwner(this);
        _lands.add(land);
    }

    /**
     * Изъять из владений указанный земельный участок.
     * @param land земельный участок
     * @return возможно ли забрать указанный земельный участок у владельца.
     */
    public boolean removeLand(ILand land) {
        if (land.owner() != this || _lands.remove(land) == false) {
            return false;
        }
        land.setOwner(null);

        return true;
    }

    /**
     * Получить все земельный владения. 
     * @return список земельных участков у игрока.
     */
    public List<ILand> landowning() {
        return _lands;
    }

    /**
     * Получить сумму денег, которой обладает игрок.
     * @return сумма денег.
     */
    public int money() {
        return _money;
    }

    /**
     * Добавить денежную сумму игроку.
     * @param amount добавляемая денежная сумма.
     * @return флаг, показываюший была ли добавлена указанная сумма.
     */
    public boolean addMoney(int amount) {
        if (amount < 0) {
            return false;
        }

        _money += amount;
        return true;
    }

    /**
     * Забрать денежную сумму у игрока.
     * @param amount изымаемая денежная сумма.
     * @return флаг, показываюший была ли изъята указанная сумма.
     */
    public boolean takeMoney(int amount) {
        if (amount < 0 || _money < amount) {
            return false;
        }

        _money -= amount;
        return true;
    }

    /**
     * Строкове представление игрока.
     * @return строка.
     */
    @Override
    public String toString() {
        return _name;
    }
}
