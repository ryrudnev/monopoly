/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.resource;

import monopoly.model.personality.ILandOwner;

/**
 * Определяет абстракцию игрового земельного участка.
 * @author Роман
 */
public interface ILand extends IGameResource {

    /**
     * Получить наименование участка земли.
     * @return наименование.
     */
    public String name();

    /**
     * Получить владельца земельного участка.
     * @return владельц земельного участка.
     */
    public ILandOwner owner();

    /**
     * Задать владельца земельного участка.
     * @param owner владелец земельного участка.
     */
    public void setOwner(ILandOwner owner);

    /**
     * Получить стоимость, которая вернется владельцу за земельного участка 
     * в случае ее конфискации.
     * @return стоимость конфискации.
     */
    public int confiscationCost();

    /**
     * Получить ренту земельного участка.
     * @return рента.
     */
    public int rent();
}
