/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.resource;

/**
 * Определяет абстракцию игрового ресурса.
 * @author Роман
 */
public interface IGameResource {

    /**
     * Получить стоимость ресурса.
     * @return стоимость ресурса.
     */
    public int cost();
}
