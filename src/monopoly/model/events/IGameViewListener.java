/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.events;

import java.util.EventListener;

/**
 * Определяет абстрактного слушателя действий представления игры.
 * @author Роман
 */
public interface IGameViewListener extends EventListener {

    /**
     * Обработчик события - начать новую игру.
     * @param e аргументы события.
     */
    public void gemeViewNewStartedGame(GameViewEvent e);

    /**
     * Обработчик события - сделать следующий ход игры.
     * @param e аргументы события.
     */
    public void gameViewMadeNextMove(GameViewEvent e);

    /**
     * Обработчик события - сделать игроку подарок.
     * @param e аргументы события.
     */
    public void gameViewMadePlayerGift(GameViewEvent e);

    /**
     * Обработчик события - оштрафовать игрока.
     * @param e аргументы события.
     */
    public void gameViewMadePlayerPenalty(GameViewEvent e);

    /**
     * Обработчик события - купить фирму игроку.
     * @param e аргументы события.
     */
    public void gameViewMadePlayerBuyLand(GameViewEvent e);

    /**
     * Обработчик события - залатить игроку ренту.
     * @param e аргументы события.
     */
    public void gameViewMadePlayerPayRent(GameViewEvent e);
}
