/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.events;

import java.util.EventObject;
import monopoly.model.personality.Player;
import monopoly.model.resource.ILand;

/**
 * Определяет событие связаное с действиями представления игры.
 * @author Роман
 */
public class GameViewEvent extends EventObject {

    /**
     * Игрок.
     */
    private Player _player = null;
    /**
     * Денежная сумма.
     */
    private int _amount = 0;
    /**
     * Количество игроков.
     */
    private int _playerCount = 0;
    /**
     * Земельный участок.
     */
    private ILand _land = null;

    /**
     * Задать аргументы события на действия представления игры.
     * @param source источник события.
     */
    public GameViewEvent(Object source) {
        super(source);
    }

    /**
     * Задать аргументы события на действия представления игры.
     * @param source источник события.
     * @param playerCount количество игроков.
     */
    public GameViewEvent(Object source, int playerCount) {
        super(source);
        
        _playerCount = playerCount;
    }

    /**
     * Задать аргументы события на действия представления игры.
     * @param source источник события.
     * @param p игрок.
     * @param amount денежная сумма.
     */
    public GameViewEvent(Object source, Player p, int amount) {
        this(source);

        _player = p;
        _amount = amount;
    }

    /**
     * Задать аргументы события на действия представления игры.
     * @param source источник события.
     * @param p игрок.
     * @param land земельный участок.
     */
    public GameViewEvent(Object source, Player p, ILand land) {
        this(source);

        _player = p;
        _land = land;
    }

    /**
     * Задать аргументы события на действия представления игры.
     * @param source источник события.
     * @param p игрок.
     * @param land земельный участок.
     * @param amount сумма сделки.
     */
    public GameViewEvent(Object source, Player p, ILand land, int amount) {
        this(source, p, amount);

        _land = land;
    }

    /**
     * Получить игрока.
     * @return игрок.
     */
    public Player getPlayer() {
        return _player;
    }

    /**
     * Получить денежную сумму.
     * @return денежная сумма.
     */
    public int getAmount() {
        return _amount;
    }

    /**
     * Получить земельный участок.
     * @return земельный участок.
     */
    public ILand getLand() {
        return _land;
    }
    
    /**
     * Получить количество игроков.
     * @return количество игроков.
     */
    public int getPlayerCount() {
        return _playerCount;
    }
}
