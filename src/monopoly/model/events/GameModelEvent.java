/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.events;

import java.util.EventObject;
import java.util.List;
import monopoly.model.game.Dice.DiceScore;
import monopoly.model.gamefield.GameFieldCellBase;
import monopoly.model.personality.ILandOwner;
import monopoly.model.personality.Player;
import monopoly.model.resource.ILand;

/**
 * Определяет событие связаное с действиями модели игры.
 * @author Роман
 */
public class GameModelEvent extends EventObject {

    /**
     * Игрок.
     */
    private Player _player = null;
    /**
     * Количество очков, выпавших на игровых костях.
     */
    private DiceScore _score = null;
    /**
     * Старая позиция игрока на игровом поле.
     */
    private GameFieldCellBase _playerOldPosition = null;
    /**
     * Новая позиция игрока на игровом поле.
     */
    private GameFieldCellBase _playerNewPosition = null;
    /**
     * Денежная сумма.
     */
    private int _amount = 0;
    /**
     * Конфискованные земельные участки.
     */
    private List<ILand> _confiscated = null;
    /**
     * Земельный участок.
     */
    private ILand _land = null;
    /**
     * Старый владелец.
     */
    private ILandOwner _oldOwner = null;
    /**
     * Новый владелец.
     */
    private ILandOwner _newOwner = null;

    /**
     * Задать аргументы события на действия модели игры.
     * @param source источник события.
     */
    public GameModelEvent(Object source) {
        super(source);
    }

    /**
     * Задать аргументы события на действия модели игры.
     * @param source источник события.
     * @param p игрок. 
     */
    public GameModelEvent(Object source, Player p) {
        super(source);

        _player = p;
    }

    /**
     * Задать аргументы события на действия модели игры.
     * @param source источник события.
     * @param p игрок.
     * @param score количество выпавших очков.
     */
    public GameModelEvent(Object source, Player p, DiceScore score) {
        this(source, p);

        _score = score;
    }

    /**
     * Задать аргументы события на действия модели игры.
     * @param source источник события.
     * @param p игрок.
     * @param playerOldPosition старая позиция игрока.
     * @param playerNewPosition новая позиция игрока.
     */
    public GameModelEvent(Object source, Player p,
            GameFieldCellBase playerOldPosition,
            GameFieldCellBase playerNewPosition) {
        this(source, p);

        _playerOldPosition = playerOldPosition;
        _playerNewPosition = playerNewPosition;
    }

    /**
     * Задать аргументы события на действия модели игры.
     * @param source источник события.
     * @param p игрок.
     * @param land земельный участок.
     */
    public GameModelEvent(Object source, Player p, ILand land) {
        this(source, p);

        _land = land;
    }

    /**
     * Задать аргументы события на действия модели игры.
     * @param source источник события.
     * @param p игрок.
     * @param amount денежная сумма.
     */
    public GameModelEvent(Object source, Player p, int amount) {
        this(source, p);

        _amount = amount;
    }

    /**
     * Задать аргументы события на действия модели игры.
     * @param source источник события.
     * @param p игрок.
     * @param confiscated конфискованные земельные участки.
     */
    public GameModelEvent(Object source, Player p, List<ILand> confiscated) {
        this(source, p);

        _confiscated = confiscated;
    }

    /**
     * Задать аргументы события на действия модели игры.
     * @param source источник события.
     * @param land земельный участок.
     * @param oldOwner старый владелец.
     * @param newOwner новый владелец.
     */
    public GameModelEvent(Object source, ILand land,
            ILandOwner oldOwner, ILandOwner newOwner) {
        this(source);

        _land = land;
        _oldOwner = oldOwner;
        _newOwner = newOwner;
    }

    /**
     * Задать аргументы события на действия модели игры.
     * @param source источник события.
     * @param p игрок.
     * @param land земельный участок.
     * @param amount сумма сделки.
     */
    public GameModelEvent(Object source, Player p, ILand land,
            int amount) {
        this(source, p, amount);

        _land = land;
    }

    /**
     * Получить количество выпавших очков на игровых костях игроком.
     * @return количество выпавших очков на игровых костях.
     */
    public DiceScore getScore() {
        return _score;
    }

    /**
     * Получить игрока.
     * @return игрок.
     */
    public Player getPlayer() {
        return _player;
    }

    /**
     * Получить старую позицию текущего игрока.
     * @return ячейка игрового поля.
     */
    public GameFieldCellBase getPlayerOldPosition() {
        return _playerOldPosition;
    }

    /**
     * Получить новую позицию текущего игрока.
     * @return ячейка игрового поля.
     */
    public GameFieldCellBase getPlayerNewPosition() {
        return _playerNewPosition;
    }

    /**
     * Получить денежную сумму.
     * @return денежная сумма.
     */
    public int getAmount() {
        return _amount;
    }

    /**
     * Получить земельный участок.
     * @return земельный участок.
     */
    public ILand getLand() {
        return _land;
    }

    /**
     * Получить старого владельца земельного участка.
     * @return владелец земельного участка.
     */
    public ILandOwner getOldOwner() {
        return _oldOwner;
    }

    /**
     * Получить нового владельца земельного участка.
     * @return владелец земельного участка.
     */
    public ILandOwner getNewOwner() {
        return _newOwner;
    }

    /**
     * Получить конфискованные земельные участки.
     * @return конфискованные земельные участки.
     */
    public List<ILand> getConfiscated() {
        return _confiscated;
    }
}
