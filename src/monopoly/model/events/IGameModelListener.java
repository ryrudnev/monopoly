/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.events;

import java.util.EventListener;

/**
 * Определяет абстрактного слушателя действий модели игры.
 * @author Роман
 */
public interface IGameModelListener extends EventListener {

    /**
     * Обработчик события - игрок бросил игровые кости.
     * @param e аргументы события.
     */
    public void gamePlayerDroppedDice(GameModelEvent e);

    /**
     * Обработчик события - игрок переместился на игровом поле.
     * @param e аргументы события.
     */
    public void gamePlayerShiftedOnField(GameModelEvent e);

    /**
     * Обработчик события - игрок начал выполнять действие.
     * Инициирует взаимодействие с пользователем через диалоговое 
     * окно представления, отображая возможные действие игрока согласно логике 
     * конкретной ячейке игрового поля.
     * @param e аргументы события.
     */
    public void gamePlayerStartedDoAction(GameModelEvent e);

    /**
     * Обработчик события - игрок оштрафован.
     * @param e аргументы события.
     */
    public void gamePlayerReceivedPenalty(GameModelEvent e);

    /**
     * Обработчик события - игрок получил подарок.
     * @param e аргументы события.
     */
    public void gamePlayerReceivedGift(GameModelEvent e);

    /**
     * Обработчик события - у игрока была конфискована земельная собственность.
     * @param e аргументы события.
     */
    public void gamePlayerLandsConfiscated(GameModelEvent e);

    /**
     * Обработчик события - игрок приобрел земельную собственность.
     * @param e аргументы события.
     */
    public void gamePlayerBoughtLand(GameModelEvent e);

    /**
     * Обработчик события - игрок уплатил ренту.
     * @param e аргументы события.
     */
    public void gamePlayerPaidRent(GameModelEvent e);

    /**
     * Обработчик события - игрок закончил игру(банкрот).
     * @param e аргументы события.
     */
    public void gamePlayerFinishedGame(GameModelEvent e);

    /**
     * Обработчик события - у земельного участка сменился владелец.
     * @param e аргументы события.
     */
    public void gameLandChangedOwner(GameModelEvent e);
    
    /**
     * Обработчик события - игра начата.
     * @param e аргументы события.
     */
    public void gameStarted(GameModelEvent e);
    
    /**
     * Обработчик события - игра закончина и выявлен победитель.
     * @param e аргументы события. 
     */
    public void gameFinished(GameModelEvent e);
}
