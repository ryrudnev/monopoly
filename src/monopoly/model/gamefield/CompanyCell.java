/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.gamefield;

import monopoly.model.personality.ILandOwner;
import monopoly.model.resource.ILand;

/**
 * Определяет игровую ячейку типа "Компания".
 * @author Роман
 */
public class CompanyCell extends GameFieldCellBase implements ILand {

    /**
     * Наименование компании.
     */
    private final String _name;
    /**
     * Стоимость фирмы.
     */
    private final int _cost;
    /**
     * Стоимость конфискации компании.
     */
    private final int _confiscationCost;
    /**
     * Рента.
     */
    private final int _rent;
    /**
     * Владелец компании.
     */
    private ILandOwner _owner;

    /**
     * Задать ячейку типа "Компания".
     * @param name наименование компании.
     * @param cost стоимость компании.
     * @param confiscationCost стоимость конфискации.
     * @param rent рента.
     */
    public CompanyCell(String name, int cost, int confiscationCost, int rent) {
        _name = name;
        _cost = cost;
        _confiscationCost = confiscationCost;
        _rent = rent;
    }

    /**
     * Получить владельца компании.
     * @return владельц компании.
     */
    public ILandOwner owner() {
        return _owner;
    }

    /**
     * Задать владельца компании.
     * @param owner владелец компании.
     */
    public void setOwner(ILandOwner owner) {
        _owner = owner;
    }

    /**
     * Получить стоимость, которая вернется владельцу за земельного участка 
     * в случае ее конфискации.
     * @return стоимость конфискации.
     */
    public int confiscationCost() {
        return _confiscationCost;
    }

    /**
     * Получить ренту компании.
     * @return рента.
     */
    public int rent() {
        return _rent;
    }

    /**
     * Получить наименование компании.
     * @return наименование.
     */
    public String name() {
        return _name;
    }

    /**
     * Получить стоимость компании.
     * @return стоимость
     */
    public int cost() {
        return _cost;
    }
}
