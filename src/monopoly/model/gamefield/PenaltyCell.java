/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.gamefield;

import java.util.Random;

/**
 * Определяет игровую ячейку типа "Штраф".
 * @author Роман
 */
public class PenaltyCell extends GameFieldCellBase {

    /**
     * Получить текущее значение денежной суммы штрафа.
     * @return значение денежной суммы штрафа.
     */
    public int currentPenalty() {
        return new Random().nextInt(15000 + 1) + 5000;
    }
}
