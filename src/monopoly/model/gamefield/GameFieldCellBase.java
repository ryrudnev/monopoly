/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.gamefield;

/**
 * Определяет абстракцию ячейки игрового поля.
 * @author Роман
 */
public abstract class GameFieldCellBase {

    /**
     * Следующая за данной ячейка игрового поля.
     */
    private GameFieldCellBase _next;

    /**
     * Получить следующую за данной ячейку игрового поля.
     * @return следующая ячейка.
     */
    public GameFieldCellBase next() {
        return _next;
    }

    /**
     * Задать следующую ячейку.
     * @param next следующая ячейка.
     */
    public void setNext(GameFieldCellBase next) {
        _next = next;
    }
}
