/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.gamefield;

import java.util.ArrayList;
import java.util.List;
import monopoly.model.personality.Player;
import monopoly.model.resource.ILand;

/**
 * Определяет игровое поле.
 * @author Роман
 */
public class GameField {

    /**
     * Максимальное количество игроков, которые могут находится на игровом поле.
     */
    public static int MAX_PLAYERS = 10;
    /**
     * Игроки, находящиеся на поле.
     */
    private final List<Player> _players;
    /**
     * Ячейки из которого состоит игровое поле.
     */
    private final List<GameFieldCellBase> _cells;

    /**
     * Задать игровое поле.
     */
    public GameField() {
        _cells = new ArrayList<GameFieldCellBase>();
        _players = new ArrayList<Player>();
    }

    /**
     * Добавить игрока на указанную позицию.
     * @param cell ячейка.
     * @param p новый игрок.
     * @return успешность добавления.
     */
    public boolean addPlayer(GameFieldCellBase cell, Player p) {
        if (_players.size() > MAX_PLAYERS
                || _players.contains(p)) {
            return false;
        }

        if (!_cells.contains(cell)) {
            return false;
        }

        p.setPosition(cell);
        return _players.add(p);
    }

    /**
     * Удалить указанного игрока с игрового поля.
     * @param p игрок.
     * @return успешность удаления.
     */
    public boolean removePlayer(Player p) {
        return _players.remove(p);
    }

    /**
     * Получить всех игроков, находящихся на игровом поле.
     * @return игроки.
     */
    public List<Player> players() {
        return _players;
    }

    /**
     * Получить всех игроков, находящихся на указанной ячейки игрового поля.
     * @param cell ячейка.
     * @return игроки.
     */
    public List<Player> players(GameFieldCellBase cell) {
        List<Player> players = new ArrayList<Player>();
        if (!_cells.contains(cell)) {
            return _players;
        }

        for (Player p : _players) {
            if (p.position().equals(cell)) {
                players.add(p);
            }
        }

        return players;
    }

    /**
     * Добавить ячейку на игровое поле.
     * @param cell ячейка.
     * @return успешность добавления.
     */
    public boolean addCell(GameFieldCellBase cell) {
        if (_cells.contains(cell)) {
            return false;
        }

        return _cells.add(cell);
    }

    /**
     * Получить ячейку по указанному индексу.
     * @param index индекс.
     * @return указанная ячейка или null.
     */
    public GameFieldCellBase cell(int index) {
        GameFieldCellBase cell = null;

        try {
            cell = _cells.get(index);
        } catch (IndexOutOfBoundsException ex) {
        }

        return cell;
    }

    /**
     * Получить все ячейки игрового поля.
     * @return ячейки.
     */
    public List<GameFieldCellBase> cells() {
        return _cells;
    }

    /**
     * Получить все ячейки, которые являются земельными участками.
     * @return земельные участки.
     */
    public List<ILand> lands() {
        List<ILand> lands = new ArrayList<ILand>();
        for (GameFieldCellBase c : _cells) {
            if (c instanceof ILand) {
                lands.add((ILand) c);
            }
        }

        return lands;
    }

    /**
     * Очистить игровое поле.
     */
    public void clear() {
        _players.clear();
        _cells.clear();
    }
}
