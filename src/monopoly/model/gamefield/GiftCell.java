/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.model.gamefield;

import java.util.Random;

/**
 * Определяет игровую ячейку типа "Подарок".
 * @author Роман
 */
public class GiftCell extends GameFieldCellBase {

    /**
     * Получить текущее значение денежной суммы подарка.
     * @return значение денежной суммы подарка.
     */
    public int currentGift() {
        return new Random().nextInt(20000 + 1) + 5000;
    }
}