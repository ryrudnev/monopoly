/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly;

import monopoly.model.game.GameModel;
import monopoly.view.GameView;
import monopoly.сontroller.GameController;

/**
 * Определяет игру монополия.
 * @author Роман
 */
public class Monopoly {

    /**
     * Товчка входа.
     * @param args аргументы командной строки.
     */
    public static void main(String[] args) {
        GameModel model = new GameModel();
        GameView view = new GameView(model);
        GameController controller = new GameController(model, view);

        view.setVisible(true);
    }
}