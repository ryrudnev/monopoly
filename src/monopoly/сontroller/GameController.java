/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.сontroller;

import monopoly.model.events.GameViewEvent;
import monopoly.model.events.IGameViewListener;
import monopoly.model.game.GameModel;
import monopoly.view.GameView;

/**
 * Определяет контроллер для взаимодействия модели и представления.
 * @author Роман
 */
public class GameController {

    /**
     * Модель игры.
     */
    private final GameModel _model;
    /**
     * Представление игры.
     */
    private final GameView _view;

    /**
     * Задать контроллер.
     * @param model модель игры.
     * @param view модель представления.
     */
    public GameController(GameModel model, GameView view) {
        _model = model;
        _view = view;

        _model.addGameModelListener(_view);

        _view.addGameViewListener(new IGameViewListener() {

            public void gameViewMadeNextMove(GameViewEvent e) {
                _model.makeNextMove();
            }

            public void gameViewMadePlayerGift(GameViewEvent e) {
                _model.makePlayerGift(e.getPlayer(), e.getAmount());
            }

            public void gameViewMadePlayerPenalty(GameViewEvent e) {
                _model.makePlayerPenalty(e.getPlayer(), e.getAmount());
            }

            public void gameViewMadePlayerBuyLand(GameViewEvent e) {
                _model.makePlayerBuyLand(e.getPlayer(),
                        e.getLand(), e.getAmount());
            }

            public void gameViewMadePlayerPayRent(GameViewEvent e) {
                _model.makePlayerPayRent(e.getPlayer(), e.getLand());
            }

            public void gemeViewNewStartedGame(GameViewEvent e) {
                _model.start(e.getPlayerCount());
            }
        });
    }
}
