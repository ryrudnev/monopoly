/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.view;

import external.HTMLColors;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import monopoly.model.personality.Player;
import monopoly.model.resource.ILand;

/**
 * Определяет панель информации о игроках.
 * @author Роман
 */
public class PlayerDetailsPanel extends JPanel {

    private static final long serialVersionUID = 10L;
    /**
     * Ширина панели.
     */
    public static int PLAYER_DETAIL_WIDTH = 80;
    /**
     * Высота панели.
     */
    public static int PLAYER_DETAIL_HEIGHT = 85;

    /**
     * Определяет информацию о игроке.
     */
    public class PlayerDetail extends JComponent {

        private static final long serialVersionUID = 11L;
        /**
         * Игрок.
         */
        private Player _player;

        /**
         * Показать подробную информацию о игроке.
         */
        public void showPlayerInfo() {
            final Dialog infoDialog = new JDialog();
            infoDialog.setModal(true);
            infoDialog.setTitle(_player.name());
            infoDialog.setLayout(new GridBagLayout());

            GridBagConstraints c = new GridBagConstraints();
            c.fill = GridBagConstraints.BOTH;
            c.insets = new Insets(5, 5, 5, 5);
            c.ipady = 5;
            c.ipadx = 5;

            c.gridx = 0;
            c.gridy = 0;
            infoDialog.add(new JLabel("<html>"
                    + "Имя игрока: <font color="
                    + HTMLColors.getName(_player.color()) + ">"
                    + _player.name() + "</font>"
                    + "</html>"), c);

            c.gridy = 1;
            infoDialog.add(new JLabel("<html>"
                    + "Обладает капиталом: <i>"
                    + _player.capital() + "$</i>"
                    + "</html>"), c);

            c.gridy = 2;
            infoDialog.add(new JLabel("<html>"
                    + "Обладает денежной суммой: <i>"
                    + _player.money() + "$</i>"
                    + "</html>"), c);

            c.gridy = 3;
            c.ipady = 0;
            c.ipadx = 0;
            infoDialog.add(new JLabel("Владеет следующими фирмами: "), c);

            List<String> companyNames = new ArrayList<String>();
            for (ILand company : _player.landowning()) {
                companyNames.add(company.name());
            }

            JList list = new JList(companyNames.toArray());
            list.setLayoutOrientation(JList.VERTICAL);
            list.setVisibleRowCount(0);

            JScrollPane scroll = new JScrollPane(list);
            scroll.setPreferredSize(new Dimension(100, 100));

            c.gridy = 4;
            c.ipady = 5;
            c.ipadx = 5;
            infoDialog.add(scroll, c);

            JButton bttn = new JButton("OK");
            bttn.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            bttn.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    infoDialog.dispose();
                }
            });

            c.fill = GridBagConstraints.NONE;
            c.gridy = 6;
            c.ipady = 0;
            c.ipadx = 0;
            infoDialog.add(bttn, c);

            infoDialog.pack();
            infoDialog.setLocationRelativeTo(this);
            infoDialog.setResizable(false);
            infoDialog.setVisible(true);
        }

        /**
         * Задать информацю о игроке.
         * @param p игрок.
         */
        public PlayerDetail(Player p) {
            super();

            setPreferredSize(new Dimension(PLAYER_DETAIL_WIDTH,
                    PLAYER_DETAIL_HEIGHT));

            _player = p;

            addMouseListener(new MouseAdapter() {

                @Override
                public void mousePressed(MouseEvent e) {
                    showPlayerInfo();
                }
            });

        }

        /**
         * Получить игрока.
         * @return игрок.
         */
        public Player player() {
            return _player;
        }

        /**
         * Отрисовать графически информацию о игроке.
         * @param g графический контекст.
         */
        @Override
        public void paint(Graphics g) {
            super.paint(g);

            int arc = 15;
            int offset = 1;
            int x = 0, y = 0;

            Graphics2D g2d = (Graphics2D) g;
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            GradientPaint gp = new GradientPaint(0, getHeight(),
                    HTMLColors.lightgreen.brighter().brighter(), 0, 0,
                    HTMLColors.lightgreen.darker());

            g.setColor(HTMLColors.white);
            g.fillRoundRect(x, y, getWidth(), getHeight(), arc, arc);

            g2d.setPaint(gp);
            g2d.fillRoundRect(offset, offset, getWidth() - (offset * 2),
                    getHeight() - (offset * 2), arc, arc);

            x = PLAYER_DETAIL_WIDTH / 4;
            y = PLAYER_DETAIL_HEIGHT / 4;

            int indent = _player.name().length();
            g.setFont(new Font("Arial", Font.BOLD, 14));
            g.setColor(_player.color());
            g.drawString(_player.name(), x - indent, y);

            indent = (_player.money() + "$").length();
            g.setFont(new Font("Arial", Font.BOLD, 12));
            g.setColor(HTMLColors.black);
            g.drawString(_player.money() + "$", x - indent, 2 * y);

            g.setFont(new Font("Arial", Font.BOLD, 10));
            
            g.drawString("капитал: ", x, 3 * y);
            g.drawString(_player.capital() + "$", x, 3 * y + 10);

            if (_player.capital() == 0) {
                g.setColor(HTMLColors.red);
                g.drawLine(3, 3, getWidth() - 3, getHeight() - 3);
                g.drawLine(3, getHeight() - 3, getWidth() - 3, 3);
            }
        }
    }
    /**
     * Информация о игроках.
     */
    private List<PlayerDetail> _details;

    /**
     * Задать панель информации о игроках.
     */
    public PlayerDetailsPanel() {
        super();

        setOpaque(false);

        _details = new ArrayList<PlayerDetail>();
    }

    /**
     * Добавить игрока.
     * @param p игрок.
     */
    public void addPlayer(Player p) {
        if (detail(p) == null) {
            PlayerDetail pd = new PlayerDetail(p);

            _details.add(pd);
            add(pd);
        }
    }

    /**
     * Удалить игрока.
     * @param p игрок.
     */
    public void removePlayer(Player p) {
        PlayerDetail pd = detail(p);
        if (pd != null) {
            _details.remove(pd);
            remove(pd);

            revalidate();
            repaint();
        }
    }

    /**
     * Очистить панель информации.
     */
    public void clear() {
        _details.clear();
        removeAll();

        revalidate();
        repaint();
    }

    /**
     * Найти иформацию о указанном игроке.
     * @param p игрок.
     * @return информация о игроке или null.
     */
    public PlayerDetail detail(Player p) {
        for (PlayerDetail pd : _details) {
            if (pd.player() == p) {
                return pd;
            }
        }

        return null;
    }

    /**
     * Отрисовать графически информацию о игроках.
     * @param g графический контекст.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int arc = 15;
        int offset = 1;

        g.setColor(HTMLColors.black);
        for (int i = 0; i < _details.size(); i++) {
            g.fillRoundRect(
                    _details.get(i).getX() - offset,
                    _details.get(i).getY() - offset,
                    _details.get(i).getWidth() + (offset * 2),
                    _details.get(i).getHeight() + (offset * 2),
                    arc,
                    arc);
        }

        super.paintChildren(g);
    }
}
