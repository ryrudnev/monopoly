/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.view;

import external.HTMLColors;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JComponent;
import monopoly.model.game.Dice.DiceScore;

/**
 * Определяет табло в виде игровых костей, 
 * отображающее количество выпавших очков.
 * @author Роман
 */
public class Scoreboard extends JComponent {

    private static final long serialVersionUID = 12L;
    /**
     * Высота.
     */
    public static int SCOREBOARD_HEIGHT = 90;
    /**
     * Ширина.
     */
    public static int SCOREBOARD_WIDTH = 220;
    /**
     * Количество очков.
     */
    private DiceScore _score = null;
    /**
     * Размеры игровой кости.
     */
    private final Dimension _diceSize;
    /**
     * Размеры точки игровой кости.
     */
    private final Dimension _diceDot;
    /**
     * Размеры границ игровой кости.
     */
    private final Dimension _diceArc;

    /**
     * Отрисовать игровую кость.
     * @param g графический контекст.
     * @param x расположение игровой кости по х.
     * @param y расположение игровой кости по у.
     * @param score очки на игровой кости.
     */
    private void paintDice(Graphics g, int x, int y, int score) {
        g.setColor(HTMLColors.limegreen);
        g.fillRoundRect(x, y, _diceSize.width - 1, _diceSize.height - 1,
                _diceArc.width, _diceArc.height);

        g.setColor(Color.BLACK);
        g.drawRoundRect(x, y, _diceSize.width - 1, _diceSize.height - 1,
                _diceArc.width, _diceArc.height);

        g.setColor(Color.BLACK);
        g.drawRoundRect(x + 1, y + 1, _diceSize.width - 3, _diceSize.height - 3,
                _diceArc.width, _diceArc.height);

        int width, height;
        width = height = _diceDot.height * 2 / 3;
        int left = x, center = x, right = x;
        left += _diceSize.width * 1 / 3 - _diceDot.width / 2 - width / 4;
        center += _diceSize.width * 2 / 3 - _diceDot.width / 2 - width / 2;
        right += _diceSize.width * 3 / 3 - _diceDot.width / 2 - width * 3 / 4;

        int top = y, middle = y, bottom = y;
        top += _diceSize.height * 1 / 3 - _diceDot.height / 2 - height / 4;
        middle += _diceSize.height * 2 / 3 - _diceDot.height / 2 - height / 2;
        bottom += _diceSize.height * 3 / 3 - _diceDot.height / 2 - height * 3 / 4;

        g.setColor(Color.BLACK);
        switch (score) {
            case 0:
                break;
            case 1:
                g.fillOval(center, middle, width, height);
                break;
            case 2:
                g.fillOval(right, top, width, height);
                g.fillOval(left, bottom, width, height);
                break;
            case 3:
                g.fillOval(right, top, width, height);
                g.fillOval(center, middle, width, height);
                g.fillOval(left, bottom, width, height);
                break;
            case 4:
                g.fillOval(left, top, width, height);
                g.fillOval(left, bottom, width, height);
                g.fillOval(right, top, width, height);
                g.fillOval(right, bottom, width, height);
                break;
            case 5:
                g.fillOval(left, top, width, height);
                g.fillOval(left, bottom, width, height);
                g.fillOval(right, top, width, height);
                g.fillOval(right, bottom, width, height);
                g.fillOval(center, middle, width, height);
                break;
            case 6:
                g.fillOval(left, top, width, height);
                g.fillOval(left, middle, width, height);
                g.fillOval(left, bottom, width, height);
                g.fillOval(right, top, width, height);
                g.fillOval(right, middle, width, height);
                g.fillOval(right, bottom, width, height);
                break;
        }
    }

    /**
     * Задать табло очков. 
     */
    public Scoreboard() {
        super();

        setPreferredSize(new Dimension(SCOREBOARD_WIDTH, SCOREBOARD_HEIGHT));
        setBackground(HTMLColors.lightgreen);

        _diceSize = new Dimension(SCOREBOARD_HEIGHT, SCOREBOARD_HEIGHT);
        _diceDot = new Dimension(_diceSize.width / 3, _diceSize.height / 3);
        _diceArc = new Dimension((int) Math.sqrt(_diceSize.width),
                (int) Math.sqrt(_diceSize.height));

    }

    /**
     * Задать количество очков.
     * @param score количество очков, выпавших на игровых костях.
     */
    public void setScore(DiceScore score) {
        _score = score;

        repaint();
    }

    /**
     * Получить количество очков.
     * @return количество очков, выпавших на игровых костях.
     */
    public DiceScore score() {
        return _score;
    }

    /**
     * Отрисовать графически табло.
     * @param g графический контекст.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int fScore = 6, sScore = 6;
        if (_score != null) {
            fScore = _score.firstScore();
            sScore = _score.secondScore();
        }

        paintDice(g, 0, 0, fScore);
        paintDice(g, getWidth() - _diceSize.width, 0, sScore);
    }
}
