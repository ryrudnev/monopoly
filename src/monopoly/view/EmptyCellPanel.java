/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.view;

import monopoly.model.gamefield.EmptyCell;

/**
 * Определяет визуальное представление ячейки типа "Пусто" на игровом поле.
 * @author Роман
 */
public class EmptyCellPanel extends CellBasePanel {

    private static final long serialVersionUID = 3L;

    /**
     * Задать представление "Пусто".
     * @param cell ячейка.
     */
    public EmptyCellPanel(EmptyCell cell) {
        super(cell);
    }
}
