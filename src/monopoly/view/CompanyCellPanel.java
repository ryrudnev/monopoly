/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.view;

import external.HTMLColors;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import monopoly.model.gamefield.CompanyCell;
import monopoly.model.personality.ILandOwner;
import monopoly.model.personality.Player;

/**
 * Определяет визуальное представление ячейки типа "Фирма" на игровом поле.
 * @author Роман
 */
public class CompanyCellPanel extends CellBasePanel {

    private static final long serialVersionUID = 2L;

    /**
     * Показать подробную информацию о компаниию.
     */
    public void showCompanyInformation() {
        CompanyCell company = (CompanyCell) _cell;

        final Dialog infoDialog = new JDialog();
        infoDialog.setModal(true);
        infoDialog.setTitle("Компания " + company.name());

        infoDialog.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(5, 5, 5, 5);
        c.ipady = 5;
        c.ipadx = 5;

        c.gridx = 0;
        c.gridy = 0;
        infoDialog.add(new JLabel("<html>"
                + "Наименование: <u>" + company.name()
                + "</u></html>"), c);

        c.gridy = 1;
        infoDialog.add(new JLabel("<html>"
                + "Цена: <i>" + company.cost() + " $</i>"
                + "</html>"), c);

        c.gridy = 2;
        infoDialog.add(new JLabel("<html>"
                + "Компенсация в случаи конфискации: <i>"
                + company.confiscationCost() + " $</i>"
                + "</html>"), c);

        c.gridy = 3;
        infoDialog.add(new JLabel("<html>"
                + "Арендная плата (рента): <i>"
                + company.rent() + " $</i>"
                + "</html>"), c);

        c.gridy = 4;
        Color color = HTMLColors.black;
        ILandOwner owner = company.owner();
        if (owner instanceof Player) {
            color = ((Player) owner).color();
        }

        infoDialog.add(new JLabel("<html>"
                + "Владелец: <font color="
                + HTMLColors.getName(color) + ">"
                + company.owner().name() + "</font>"
                + "</html>"), c);

        JButton bttn = new JButton("OK");
        bttn.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        bttn.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                infoDialog.dispose();
            }
        });

        c.fill = GridBagConstraints.NONE;
        c.gridx = 0;
        c.gridy = 5;
        infoDialog.add(bttn, c);

        infoDialog.pack();
        infoDialog.setLocationRelativeTo(this);
        infoDialog.setResizable(false);
        infoDialog.setVisible(true);
    }

    /**
     * Задать представление "Фирма".
     * @param cell ячейка.
     */
    public CompanyCellPanel(CompanyCell cell) {
        super(cell);

        addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                showCompanyInformation();
            }
        });
    }

    /**
     * Отрисовать графически ячейку "Подарок".
     * @param g графический контекст.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        CompanyCell company = (CompanyCell) _cell;

        int x = CELL_WIDTH / 3, y = CELL_HEIGHT - CELL_HEIGHT / 3;
        
        int indent = company.name().length();
        
        g.setColor(HTMLColors.black);
        
        g.setFont(new Font("Arial", Font.BOLD, 12));
        g.drawString(company.name(), x - indent, y - 15);
        
        g.setFont(new Font("Arial", Font.ITALIC, 12));
        g.drawString(company.cost() + "$", x - indent, y + 5);

        x = 0;
        y = CELL_HEIGHT - CELL_HEIGHT / 5;

        g.drawRect(x, y, CELL_WIDTH, CELL_HEIGHT - y);

        ILandOwner owner = company.owner();
        if (owner instanceof Player) {
            Player p = (Player) owner;

            Graphics2D g2d = (Graphics2D) g;
            GradientPaint gp = new GradientPaint(x, y, Color.WHITE,
                    x, CELL_HEIGHT, p.color().darker());
            g2d.setPaint(gp);
            g2d.fillRect(1, y + 1, CELL_WIDTH - 2, (CELL_HEIGHT - y) - 2);
        }

        super.paintChildren(g);
    }
}
