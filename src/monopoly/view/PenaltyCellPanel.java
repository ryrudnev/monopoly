/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import monopoly.model.gamefield.PenaltyCell;

/**
 * Определяет визуальное представление ячейки типа "Штраф" на игровом поле.
 * @author Роман
 */
public class PenaltyCellPanel extends CellBasePanel {

    private static final long serialVersionUID = 7L;

    /**
     * Задать представление "Штраф".
     * @param cell ячейка.
     */
    public PenaltyCellPanel(PenaltyCell cell) {
        super(cell);
    }

    /**
     * Отрисовать графически ячейку "Штраф".
     * @param g графический контекст.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int x = CELL_WIDTH / 5, y = CELL_HEIGHT - CELL_HEIGHT / 4;

        g.setFont(new Font("Arial", Font.BOLD, 16));
        g.setColor(Color.RED.darker());
        g.drawString("ШТРАФ", x, y);

        super.paintChildren(g);
    }
}
