/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import monopoly.model.gamefield.GiftCell;

/**
 * Определяет визуальное представление ячейки типа "Подарок" на игровом поле.
 * @author Роман
 */
public class GiftCellPanel extends CellBasePanel {

    private static final long serialVersionUID = 6L;

    /**
     * Задать представление "Подарок".
     * @param cell ячейка.
     */
    public GiftCellPanel(GiftCell cell) {
        super(cell);
    }

    /**
     * Отрисовать графически ячейку "Подарок".
     * @param g графический контекст.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int x = CELL_WIDTH / 5, y = CELL_HEIGHT - CELL_HEIGHT / 4;

        g.setFont(new Font("Arial", Font.BOLD, 16));
        g.setColor(Color.GREEN.darker());
        g.drawString("ПОДАРОК", x, y);

        super.paintChildren(g);
    }
}
