/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.view;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import monopoly.model.personality.Player;

/**
 * Определяет панель расположения игровых фишек.
 * @author Роман
 */
public class PlayerChipsPanel extends JPanel {

    private static final long serialVersionUID = 9L;
    /**
     * Ширина панели.
     */
    public static int PANEL_WIDTH = 100;
    /**
     * Высота панели.
     */
    public static int PANEL_HEIGHT = 30;
    /**
     * Расстояние между фишками игроков по горизонтали.
     */
    public static int CHIP_HORIZONTAL_INDENT = 5;
    /**
     * Расстояние между фишками игроков по вертикали.
     */
    public static int CHIP_VERTICAL_INDENT = 0;
    /**
     * Количество фишек игроков в одном ряду.
     */
    public static int CHIP_IN_ROW = 5;

    /**
     * Задать панель игровых фишек.
     */
    public PlayerChipsPanel() {
        super();

        setLayout(new GridLayout(0, CHIP_IN_ROW,
                CHIP_HORIZONTAL_INDENT, CHIP_VERTICAL_INDENT));
        setPreferredSize(new Dimension(PANEL_WIDTH, PANEL_HEIGHT));
        setOpaque(false);
    }

    /**
     * Получить все фишки на панели.
     * @return фишки.
     */
    public List<PlayerChip> chips() {
        List<PlayerChip> chips = new ArrayList<PlayerChip>();
        for (Component c : getComponents()) {
            if (c instanceof PlayerChip) {
                chips.add((PlayerChip) c);
            }
        }

        return chips;
    }

    /**
     * Получить фишку по игроку.
     * @param p игрок.
     * @return фишка игрока.
     */
    public PlayerChip chip(Player p) {
        List<PlayerChip> chips = chips();
        for (PlayerChip c : chips) {
            if (c.player() == p) {
                return c;
            }
        }

        return null;
    }

    /**
     * Добавить фишку.
     * @param chip фишка.
     */
    public void addChip(PlayerChip chip) {
        add(chip);

        revalidate();
        repaint();
    }

    /**
     * Удалить фишку с панели если она имеется.
     * @param chip фишка.
     */
    public void removeChip(PlayerChip chip) {
        remove(chip);

        revalidate();
        repaint();
    }

    /**
     * Очистить панель от всех фишек.
     */
    public void clear() {
        removeAll();

        revalidate();
        repaint();
    }

    /**
     * Отрисовать графически панель фишек.
     * @param g графический контекст.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D antiAlias = (Graphics2D) g;
        antiAlias.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        super.paintChildren(g);
    }
}
