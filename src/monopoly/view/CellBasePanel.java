/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;
import monopoly.model.gamefield.GameFieldCellBase;

/**
 * Определяет базовое визуальное представление ячейки на игровом поле.
 * @author Роман
 */
public abstract class CellBasePanel extends JPanel {

    private static final long serialVersionUID = 1L;
    /**
     * Ширина ячейки.
     */
    public static final int CELL_WIDTH = 110;
    /**
     * Высота ячейки.
     */
    public static final int CELL_HEIGHT = 90;
    /**
     * Игровая ячейка.
     */
    protected GameFieldCellBase _cell;
    /**
     * Панель фишек игроков, находящихся на данной ячейки.
     */
    private final PlayerChipsPanel _chipPanel = new PlayerChipsPanel();

    /**
     * Задать представление игровой ячейки.
     * @param cell игровя ячейка. 
     */
    protected CellBasePanel(GameFieldCellBase cell) {
        super();

        setPreferredSize(new Dimension(CELL_WIDTH, CELL_HEIGHT));
        add(_chipPanel);

        _cell = cell;
    }

    /**
     * Получить модель ячейки игрового поля.
     * @return ячейка игрового поля.
     */
    public GameFieldCellBase cell() {
        return _cell;
    }

    /**
     * Задать модель ячейки игровго поля.
     * @param cell ячейка игрового поля.
     */
    public void setCell(GameFieldCellBase cell) {
        _cell = cell;

        repaint();
    }

    /**
     * Получить панель игровых фишек.
     * @return панель игровых фишек. 
     */
    public PlayerChipsPanel chipsPanel() {
        return _chipPanel;
    }

    /**
     * Отрисовать графически данную ячейку.
     * @param g графический контекст.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int x = 0, y = 0;
        g.setColor(Color.WHITE);
        g.fillRect(x, y, getWidth(), getHeight());

        g.setColor(Color.BLACK);
        g.drawRect(x, y, getWidth() - 1, getHeight() - 1);

        super.paintChildren(g);
    }
}
