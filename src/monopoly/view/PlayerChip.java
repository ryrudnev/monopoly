/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.view;

import external.HTMLColors;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import monopoly.model.personality.Player;
import monopoly.model.resource.ILand;

/**
 * Определяет визуальное представление фишки игрока.
 * @author Роман
 */
public class PlayerChip extends JComponent {

    private static final long serialVersionUID = 8L;
    /**
     * Размер радиуса фишки игрока.
     */
    public static int RADIUS = 15;
    /**
     * Игрок.
     */
    private Player _player;

    /**
     * Показать подробную информацию о игроке.
     */
    public void showPlayerInfo() {
        final Dialog infoDialog = new JDialog();
        infoDialog.setModal(true);
        infoDialog.setTitle(_player.name());
        infoDialog.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(5, 5, 5, 5);
        c.ipady = 5;
        c.ipadx = 5;

        c.gridx = 0;
        c.gridy = 0;
        infoDialog.add(new JLabel("<html>"
                + "Имя игрока: <font color="
                + HTMLColors.getName(_player.color()) + ">"
                + _player.name() + "</font>"
                + "</html>"), c);

        c.gridy = 1;
        infoDialog.add(new JLabel("<html>"
                + "Обладает капиталом: <i>"
                + _player.capital() + "$</i>"
                + "</html>"), c);

        c.gridy = 2;
        infoDialog.add(new JLabel("<html>"
                + "Обладает денежной суммой: <i>"
                + _player.money() + "$</i>"
                + "</html>"), c);

        c.gridy = 3;
        c.ipady = 0;
        c.ipadx = 0;
        infoDialog.add(new JLabel("Владеет следующими фирмами: "), c);

        List<String> companyNames = new ArrayList<String>();
        for (ILand company : _player.landowning()) {
            companyNames.add(company.name());
        }

        JList list = new JList(companyNames.toArray());
        list.setLayoutOrientation(JList.VERTICAL);
        list.setVisibleRowCount(0);

        JScrollPane scroll = new JScrollPane(list);
        scroll.setPreferredSize(new Dimension(100, 100));

        c.gridy = 4;
        c.ipady = 5;
        c.ipadx = 5;
        infoDialog.add(scroll, c);

        JButton bttn = new JButton("OK");
        bttn.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        bttn.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                infoDialog.dispose();
            }
        });

        c.fill = GridBagConstraints.NONE;
        c.gridy = 6;
        c.ipady = 0;
        c.ipadx = 0;
        infoDialog.add(bttn, c);

        infoDialog.pack();
        infoDialog.setLocationRelativeTo(this);
        infoDialog.setResizable(false);
        infoDialog.setVisible(true);
    }

    /**
     * Задать фишку игрока.
     * @param player игрок.
     */
    public PlayerChip(Player player) {
        super();

        _player = player;

        addMouseListener(new MouseAdapter() {

            @Override
            public void mousePressed(MouseEvent e) {
                showPlayerInfo();
            }
        });
    }

    /**
     * Получить модель игрока.
     * @return иодель игрока.
     */
    public Player player() {
        return _player;
    }

    /**
     * Задать модель игрока.
     * @param p игрок.
     */
    public void setPlayer(Player p) {
        _player = p;

        repaint();
    }

    /**
     * Отрисовать графически фишку игрока.
     * @param g графический контекст.
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);

        int x = 0, y = 0;
        Graphics2D g2d = (Graphics2D) g;
        GradientPaint gp = new GradientPaint(x, y, Color.WHITE,
                RADIUS, RADIUS, _player.color().darker());
        g2d.setPaint(gp);
        g2d.fillOval(x, y, RADIUS, RADIUS);
    }
}
