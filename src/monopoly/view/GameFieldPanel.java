/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monopoly.view;

import external.HTMLColors;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import monopoly.model.gamefield.CompanyCell;
import monopoly.model.gamefield.EmptyCell;
import monopoly.model.gamefield.GameField;
import monopoly.model.gamefield.GameFieldCellBase;
import monopoly.model.gamefield.GiftCell;
import monopoly.model.gamefield.PenaltyCell;
import monopoly.model.personality.Player;

/**
 * Определяет представление игрового поля игры Монополии.
 * @author Роман
 */
public class GameFieldPanel extends JPanel {

    private static final long serialVersionUID = 4L;
    /**
     * Модель игрового поле.
     */
    private GameField _field;
    /**
     * Табло очков.
     */
    private final Scoreboard _scoreboard;
    /**
     * Панелт информации о игроках.
     */
    private final PlayerDetailsPanel _detailsPanel;
    /**
     * Визуальное представление ячеек, из которых состоит игровое поле.
     */
    private final List<CellBasePanel> _cellPanels;

    /**
     * Удалить все ячейки с игрового поля.
     */
    private void removeAllCells() {
        for (CellBasePanel c : _cellPanels) {
            remove(c);
        }

        _cellPanels.clear();
    }

    /**
     * Инциализировать панель информации о игроках.
     */
    private void initializePlayerDetails() {
        _detailsPanel.clear();

        for (Player p : _field.players()) {
            _detailsPanel.addPlayer(p);
        }
    }

    /**
     * Инциализировать ячейки представления игрового поля.
     */
    private void initializeCells() {
        removeAllCells();

        for (GameFieldCellBase c : _field.cells()) {
            if (c instanceof CompanyCell) {
                _cellPanels.add(new CompanyCellPanel((CompanyCell) c));
            } else if (c instanceof EmptyCell) {
                _cellPanels.add(new EmptyCellPanel((EmptyCell) c));
            } else if (c instanceof GiftCell) {
                _cellPanels.add(new GiftCellPanel((GiftCell) c));
            } else if (c instanceof PenaltyCell) {
                _cellPanels.add(new PenaltyCellPanel((PenaltyCell) c));
            }
        }

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 10;
        c.weighty = 10;

        int max = _cellPanels.size() / 4;
        int y = 0, x = 0;
        for (int i = 0; i < _cellPanels.size(); i++) {
            if (i <= max) {
                x++;
            } else if (i <= 2 * max) {
                y++;
            } else if (i <= 3 * max) {
                x--;
            } else if (i <= 4 * max) {
                y--;
            }

            c.gridx = x;
            c.gridy = y;
            add(_cellPanels.get(i), c);
        }
    }

    /**
     * Отрисовать графически поле.
     * @param g графический контекст.
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        GradientPaint gp = new GradientPaint(0, 0,
                getBackground().brighter().brighter(), 0, getHeight(),
                getBackground().darker().darker());
        g2d.setPaint(gp);
        g2d.fillRect(0, 0, getWidth(), getHeight());

        super.paintChildren(g);
    }

    /**
     * Задать представление игрового поля.
     * @param field игровое поле.
     */
    public GameFieldPanel(GameField field) {
        super();

        _field = field;
        _cellPanels = new ArrayList<CellBasePanel>();

        setBackground(HTMLColors.lightgreen);
        setLayout(new GridBagLayout());

        initializeCells();

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.gridheight = 2;
        c.gridwidth = 2;
        c.gridx = _cellPanels.size() / 8 + 1;
        c.gridy = _cellPanels.size() / 8 - 2;
        _scoreboard = new Scoreboard();
        add(_scoreboard, c);

        c.gridheight = 2;
        c.gridwidth = _cellPanels.size();
        c.gridx = 1;
        c.gridy = _cellPanels.size() / 8 + 3;
        _detailsPanel = new PlayerDetailsPanel();
        add(_detailsPanel, c);

        initializePlayerDetails();
    }

    /**
     * Получить модель игрового поля.
     * @return модель игрового поля.
     */
    public GameField gameField() {
        return _field;
    }

    /**
     * Задать модель игрового поля.
     * @param field игровое поле.
     */
    public void setGameField(GameField field) {
        _field = field;

        initializeCells();
        initializePlayerDetails();

        _scoreboard.setScore(null);
    }

    /**
     * Получить игровое табло очков.
     * @return табло очков.
     */
    public Scoreboard scoreboard() {
        return _scoreboard;
    }

    /**
     * Получить панель с информацией о игроках.
     * @return панель с информацией о игроках.
     */
    public PlayerDetailsPanel detailsPanel() {
        return _detailsPanel;
    }

    /**
     * Получить все графические представления ячеек.
     * @return графические представления ячеек.
     */
    public List<CellBasePanel> cellPanels() {
        return _cellPanels;
    }

    /**
     * Получить графическое представление ячейки по ее модели.
     * @param cell модель ячейки.
     * @return графическое представление.
     */
    public CellBasePanel cellPanel(GameFieldCellBase cell) {
        for (CellBasePanel p : _cellPanels) {
            if (p.cell() == cell) {
                return p;
            }
        }

        return null;
    }

    /**
     * Удалить фишку игрока с игрового поля.
     * @param chip фишка.
     * @return успешность удаления.
     */
    public boolean removePlayerChip(PlayerChip chip) {
        for (int i = 0; i < _cellPanels.size(); i++) {
            _cellPanels.get(i).chipsPanel().removeChip(chip);
        }

        return true;
    }
}
